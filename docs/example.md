# Example of management configuration

```
mikrotik_management:
  hostname: host1 # hostname to set for the router. Default is inventory_hostname
  telnet:
    available_from_addresses: [] # Default: []. Addresses to allow connection from.
    disabled: yes # Default: yes.
    port: 23 # Default 23. TCP port for the service.
  ftp:
    available_from_addresses: []
    disabled: yes # Default: yes.
    port: 21
  www:
    available_from_addresses: []
    disabled: yes # Default: yes.
    port: 80
  www-ssl:
    available_from_addresses: []
    disabled: yes # Default: yes.
    port: 443
    certificate: "" # TLS certificate from /certificate to use for the service
  ssh:
    available_from_addresses: []
    disabled: no # Default: no.
    port: 22
  api:
    available_from_addresses: []
    disabled: yes # Default: yes.
    port: 8728
  winbox:
    available_from_addresses: []
    disabled: yes # Default: yes.
    port: 8291
  api-ssl:
    available_from_addresses: []
    disabled: yes # Default: yes.
    port: 8729
    certificate: ""

  snmp: # Global SNMP configuration
    credentials_dir: (string) # Place to save new auto generated default snmp community name
    enabled: no # Default: no. Status of SNMP
    contact: admin@example.com # Default: none. SNMP contact info
    location: office 1 # Default: none. SNMP location
    engine-id: engine 1 # Default: none.
    trap-target:
      - 10.8.0.1 # Default: none. List of trap targets
    trap-community: public # Default: random string which is equal to "default" SNMP community name.  Must exists in RouterOS config or error.
    trap-version: 1  # Default: 1.
    trap-generators: temp-exception  # Default: temp-exception.

    communities: # SNMP communities
      - name: public  # Required
        addresses: # list of addresses to allow connection from
          - 10.8.0.1
        write-access: no # Default: no. Allow/Disallow write access
        read-access: no # Default: no. Allow/Disallow read access
        authentication-protocol: MD5  # Default: MD5.
        encryption-protocol: "DES"  # Default: DES.
        encryption-password: "" # Default: none.
        authentication-password: "" # Default: none.
        security: none # Default: none.
```
